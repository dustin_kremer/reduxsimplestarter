import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';

import reset from '../../style/reset.scss';
import main from '../../style/main.scss';

export default class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <AppBar title="ReduxSimpleStarter" />
      </MuiThemeProvider>
    );
  }
}
